//
//  ViewController.m
//  AutoLayoutDemo
//
//  Created by James Cash on 17-04-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (strong, nonatomic) IBOutlet UIView *pinkView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *pinkViewLeadingConstraint;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.pinkViewLeadingConstraint.constant = 15;

    UIView *v1 = [[UIView alloc] init];
    v1.translatesAutoresizingMaskIntoConstraints = NO;
    v1.backgroundColor = [UIColor greenColor];
    [self.view addSubview:v1];

//    NSLayoutConstraint *cons1 =
    [NSLayoutConstraint constraintWithItem:v1
                                 attribute:NSLayoutAttributeBottom
                                 relatedBy:NSLayoutRelationEqual
                                    toItem:self.pinkView
                                 attribute:NSLayoutAttributeTop
                                multiplier:1.0
                                  constant:-20].active = YES;
    //    cons1.active = YES;

    [NSLayoutConstraint constraintWithItem:v1
                                 attribute:NSLayoutAttributeWidth
                                 relatedBy:NSLayoutRelationEqual
                                    toItem:nil
                                 attribute:NSLayoutAttributeNotAnAttribute
                                multiplier:1.0
                                  constant:120].active = YES;

    [v1.centerXAnchor constraintEqualToAnchor:self.pinkView.centerXAnchor].active = YES;
    [v1.heightAnchor constraintEqualToAnchor:self.pinkView.heightAnchor multiplier:1.5].active = YES;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [UIView animateWithDuration:2 animations:^{
        if (self.pinkViewLeadingConstraint.constant == 0) {
            self.pinkViewLeadingConstraint.constant = 30;
        } else {
            self.pinkViewLeadingConstraint.constant = 0;
        }
        // If you want to animate changes to a constraint, you need to make sure that the layout engine runs *inside* the animation block
        [self.view layoutIfNeeded];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
